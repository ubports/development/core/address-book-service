# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Canonical Ltd.
# This file is distributed under the same license as the address-book-service package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: address-book-service\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-12-16 16:59+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"Language: et\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: lib/addressbook.cpp:1201
msgid "Address-book service"
msgstr ""

#: lib/addressbook.cpp:1213
msgid "Update required"
msgstr ""

#: lib/addressbook.cpp:1215
msgid ""
"Only local contacts will be editable until the contact sync upgrade is "
"complete."
msgstr ""

#: lib/addressbook.cpp:1220
msgid "Update complete"
msgstr ""

#: lib/addressbook.cpp:1222
msgid "Your contact sync upgrade is complete."
msgstr ""

#: updater/ab-notify-message.cpp:85
msgid "Yes"
msgstr ""

#: updater/ab-notify-message.cpp:90
msgid "No"
msgstr ""

#: updater/ab-update-buteo-import.cpp:306
msgid "Turn on Syncing"
msgstr ""

#: updater/ab-update-buteo-import.cpp:307
msgid "Keep turned off"
msgstr ""

#: updater/ab-update-buteo-import.cpp:312
msgid "Contact Syncing"
msgstr ""

#: updater/ab-update-buteo-import.cpp:314
#, qt-format
msgid ""
"Turn on contact-syncing for the <b>%1</b> Google-account.\n"
"Otherwise your contacts will be saved, but not synced with this account."
msgstr ""

#: updater/ab-update.cpp:103 updater/ab-update.cpp:276
#: updater/ab-update.cpp:352 updater/ab-update.cpp:363
msgid "Account update"
msgstr ""

#: updater/ab-update.cpp:104
#, qt-format
msgid "%1 contact sync account upgrade already in progress"
msgstr ""

#: updater/ab-update.cpp:225
msgid "Contacts app already updated"
msgstr ""

#: updater/ab-update.cpp:227
msgid "Could not connect to the Internet"
msgstr ""

#: updater/ab-update.cpp:229
msgid "Could not connect to contact-sync service"
msgstr ""

#: updater/ab-update.cpp:231
msgid "Could not create sync profile"
msgstr ""

#: updater/ab-update.cpp:233
msgid "Could not log in"
msgstr ""

#: updater/ab-update.cpp:235
msgid "Could not sync due to internal error"
msgstr ""

#: updater/ab-update.cpp:237
msgid "Could not find online account"
msgstr ""

#: updater/ab-update.cpp:239
msgid "Already syncing contacts…"
msgstr ""

#: updater/ab-update.cpp:242
msgid "Could not sync contacts"
msgstr ""

#: updater/ab-update.cpp:277
msgid "Contact-sync update complete."
msgstr ""

#: updater/ab-update.cpp:296 updater/ab-update.cpp:321
msgid "Update failed"
msgstr ""

#: updater/ab-update.cpp:297
#, qt-format
msgid ""
"%1.\n"
"Retry now?"
msgstr ""

#: updater/ab-update.cpp:322
msgid "Open the Contacts app and press the \"Sync\" button to retry."
msgstr ""

#: updater/ab-update.cpp:353
#, qt-format
msgid "Upgrading %1 contact-sync account(s)…"
msgstr ""

#: updater/ab-update.cpp:364
#, qt-format
msgid ""
"%1 contact-sync account needs an upgrade. Please connect to the Internet."
msgstr ""
