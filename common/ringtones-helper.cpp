/*
 * Copyright 2023 Ubports Foundation
 *
 * This file is part of contact-service-app.
 *
 * contact-service-app is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * contact-service-app is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ringtones-helper.h"

#include <QContactExtendedDetail>
#include <QContactRingtone>
#include <QDebug>

namespace galera {


RingtonesHelper::RingtonesHelper() {}

void RingtonesHelper::convertExtendedDetailToContactRingTone(QContact &contact)
{
    if (contact.isEmpty()) {
        return;
    }

    QString ringToneUrl;
    Q_FOREACH(const QContactExtendedDetail &det, contact.details(QContactDetail::TypeExtendedDetail)) {
        if (det.name() == "X-RINGTONE-URL") {
            ringToneUrl = det.data().toString();
            break;
        }
    }

    if (!ringToneUrl.isEmpty()) {
        QContactRingtone ringTone = contact.detail<QContactRingtone>();
        QContactRingtone newRingTone(ringTone);
        newRingTone.setAudioRingtoneUrl(ringToneUrl);
        contact.saveDetail(&newRingTone);
    }
}

void RingtonesHelper::convertContactRingtoneToExtendedDetail(QContact &contact)
{

    if (contact.isEmpty()) {
        return;
    }

    QContactRingtone rdet =  contact.detail(QContactDetail::TypeRingtone);

    bool found = false;
    Q_FOREACH (QContactExtendedDetail detail, contact.details(QContactDetail::TypeExtendedDetail)) {
        if (detail.name() == "X-RINGTONE-URL") {

            if (rdet.audioRingtoneUrl().toString() != detail.data().toString()) {
                detail.setData(rdet.audioRingtoneUrl().toString());
                contact.saveDetail(&detail);
            }

            found = true;
            break;
        }
    }

    if (!found && !rdet.audioRingtoneUrl().isEmpty()) {
        QContactExtendedDetail ringToneExtDetail;
        ringToneExtDetail.setName("X-RINGTONE-URL");
        ringToneExtDetail.setData(rdet.audioRingtoneUrl().toString());
        contact.saveDetail(&ringToneExtDetail);
    }
}

}
