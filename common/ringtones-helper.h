/*
 * Copyright 2023 Ubports Foundation
 *
 * This file is part of contact-service-app.
 *
 * contact-service-app is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * contact-service-app is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __GALERA_RINGTONE_HELPER_H__
#define __GALERA_RINGTONE_HELPER_H__


#include <QContact>
#include <QContact>

namespace galera
{
using namespace QtContacts;

class RingtonesHelper
{
public:
    RingtonesHelper();


    static void convertExtendedDetailToContactRingTone(QContact &contact);
    static void convertContactRingtoneToExtendedDetail(QContact &contact);

};

}

#endif
