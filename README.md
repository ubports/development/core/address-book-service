# Lomiri PIM service

A contact aggregator service, that exports all contact information through
D-Bus.

## i18n: Translating Lomiri's Address Book Service into your Language

You can easily contribute to the localization of this project (i.e. the
translation into your language) by visiting (and signing up with) the
Hosted Weblate service:
https://hosted.weblate.org/projects/lomiri/address-book-service

The localization platform of this project is sponsored by Hosted Weblate
via their free hosting plan for Libre and Open Source Projects.
